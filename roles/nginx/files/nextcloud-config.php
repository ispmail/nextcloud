  'memcache.local' => '\OC\Memcache\Redis',
  'memcache.locking' => '\OC\Memcache\Redis',
  'redis' => array (
    'host' => '/var/run/redis/redis.sock',
    'port' => 0,
    'timeout' => 0.0,
  ),

  'knowledgebaseenabled' => true,
  'allow_user_to_change_display_name' => true,
  'token_auth_enforced' => true,
  'auth.bruteforce.protection.enabled' => true,
  'mail_domain' => 'example.com',
  'mail_from_address' => 'nextcloud',
  'trashbin_retention_obligation' => 'auto',
  'versions_retention_obligation' => 'auto',
  'lost_password_link' => 'disabled',

  "user_backends" => array (
  	0 => array (
  	 "class"     => "OC_User_IMAP",
  	 "arguments" => array (
  	   0 => '{imap.gmail.com:993/imap/ssl/readonly}', 'example.org'
  			),
  		),
  	),
